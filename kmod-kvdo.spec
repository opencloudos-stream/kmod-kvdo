%define kmod_name		kvdo
%define kmod_kernel_version	6.6.6-2401.0.1.1.ocs23
%define kmod_headers_version	%(rpm -qa kernel-devel | sed 's/^kernel-devel-//')
%global kernel_source           /usr/src/kernels/%{kmod_headers_version}
%define kmod_kbuild_dir		.
%define kmod_devel_package	0
%define findpat %( echo "%""P" )

%global _use_internal_dependency_generator 0

Summary:	  Kernel Modules for VDO(Virtual Data Optimizer)
Name:		  kmod-kvdo
Version:	  8.2.3.3
Release:	  5%{?dist}
License:	  GPLv2+
URL:		  http://github.com/dm-vdo/kvdo
Source0:	  https://github.com/dm-vdo/kvdo/archive/refs/tags/%{version}.tar.gz # /%{kmod_name}-%{version}.tar.gz
Patch3000:        add_lz4_dependency.patch
Patch3001:        removed-logical-space-check-from-table-line.patch
Patch5000:        correct-warings-treated-as-errors.patch
Patch5001:        kmod-kvdo-8.2.3.3-add-support-for-loongarch64.patch
Patch5002:        0001-Don-t-export-LZ4-compress-functions.patch

BuildRequires:    gcc make elfutils-libelf-devel libuuid-devel
BuildRequires:	  kernel-devel >= %{kmod_kernel_version}
BuildRequires:    system-rpm-config
Provides:         kmod-%{kmod_name} = %{version}-%{release}
Requires(post):   %{_sbindir}/weak-modules
Requires(postun): %{_sbindir}/weak-modules
Requires:         kernel-core    >= %{kmod_kernel_version}
Requires:         kernel-modules >= %{kmod_kernel_version}

%description
Virtual Data Optimizer (VDO) is software that provides inline block-level deduplication,
compression, and thin provisioning capabilities for primary storage.
VDO installs within the Linux device mapper framework, where it takes ownership of
existing physical block devices and remaps these to new, higher-level block devices
with data-efficiency capabilities.

VDO includes two kernel modules, and a set of userspace tools for managing them. 
This package provides the kernel modules for VDO.


%prep
%setup -q -n %{kmod_name}-%{version}
%autopatch -p1 -M 3001
set -- *
mkdir source
mv "$@" source/
%patch5000 -p1
%patch5001 -p1
%patch5002 -p1
mkdir obj


%build
rm -rf obj
cp -r source obj
make -C %{kernel_source} M=$PWD/obj/%{kmod_kbuild_dir} V=1 \
	NOSTDINC_FLAGS="-I $PWD/obj/include -I $PWD/obj/include/uapi"

find obj/%{kmod_kbuild_dir} -name "*.ko" -type f -exec chmod u+x '{}' +

whitelist="/lib/modules/kabi-current/kabi_whitelist_%{_target_cpu}"

for modules in $( find obj/%{kmod_kbuild_dir} -name "*.ko" -type f -printf "%{findpat}\n" | sed 's|\.ko$||' | sort -u ) ; do
	module_weak_path=$(echo $modules | sed 's/[\/]*[^\/]*$//')
	if [ -z "$module_weak_path" ]; then
		module_weak_path=%{name}
	else
		module_weak_path=%{name}/$module_weak_path
	fi
	echo "override $(echo $modules | sed 's/.*\///') $(echo %{kmod_headers_version} | sed 's/\.[^\.]*$//').* weak-updates/$module_weak_path" >> source/depmod.conf

	nm -u obj/%{kmod_kbuild_dir}/$modules.ko | sed 's/.*U //' |  sed 's/^\.//' | sort -u | while read -r symbol; do
		grep -q "^\s*$symbol\$" $whitelist || echo "$symbol" >> source/greylist
	done
done
sort -u source/greylist | uniq > source/greylist.txt


%install
export INSTALL_MOD_PATH=$RPM_BUILD_ROOT
export INSTALL_MOD_DIR=extra/%{name}
make -C %{kernel_source} modules_install V=1 \
	M=$PWD/obj/%{kmod_kbuild_dir}
find $INSTALL_MOD_PATH/lib/modules -iname 'modules.*' -exec rm {} \;

install -m 644 -D source/depmod.conf $RPM_BUILD_ROOT/etc/depmod.d/%{kmod_name}.conf
install -m 644 -D source/greylist.txt $RPM_BUILD_ROOT/usr/share/doc/kmod-%{kmod_name}/greylist.txt


%pre
# Warning when kvdo or uds is loaded. 
for module in kvdo uds; do
  if grep -q "^${module}" /proc/modules; then
    if [ "${module}" == "kvdo" ]; then
      echo "WARNING: Found ${module} module previously loaded (Version: $(cat /sys/kvdo/version 2>/dev/null || echo Unknown)).  A reboot is recommended before attempting to use the newly installed module."
    else
      echo "WARNING: Found ${module} module previously loaded.  A reboot is recommended before attempting to use the newly installed module."
    fi
  fi
done

%post
modules=( $(find /lib/modules/%{kmod_headers_version}/extra/kmod-%{kmod_name} | grep '\.ko$') )
printf '%s\n' "${modules[@]}" >> /usr/lib/rpm-kmod-posttrans-weak-modules-add

%pretrans -p <lua>
posix.unlink("/usr/lib/rpm-kmod-posttrans-weak-modules-add")

%posttrans
if [ -f "/usr/lib/rpm-kmod-posttrans-weak-modules-add" ]; then
	modules=( $(cat /usr/lib/rpm-kmod-posttrans-weak-modules-add) )
	rm -rf /usr/lib/rpm-kmod-posttrans-weak-modules-add
	printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --dracut=/usr/bin/dracut --add-modules
fi

%preun
rpm -ql kmod-kvdo-%{version}-%{release}.$(arch) | grep '\.ko$' > /var/run/rpm-kmod-%{kmod_name}-modules
for module in kvdo uds; do
  if grep -q "^${module}" /proc/modules; then
    warnMessage="WARNING: ${module} in use.  Changes will take effect after a reboot."
    modprobe -r ${module} 2>/dev/null || echo ${warnMessage} && /usr/bin/true
  fi
done

%postun
modules=( $(cat /var/run/rpm-kmod-%{kmod_name}-modules) )
rm /var/run/rpm-kmod-%{kmod_name}-modules
printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --dracut=/usr/bin/dracut --remove-modules


%files
%defattr(644,root,root,755)
/lib/modules/%{kmod_headers_version}
/etc/depmod.d/%{kmod_name}.conf
/usr/share/doc/kmod-%{kmod_name}/greylist.txt


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.2.3.3-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Mon Sep 02 2024 Xiaojie Chen <jackxjchen@tencent.com> - 8.2.3.3-4
- Don't export LZ4 compress functions

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.2.3.3-3
- Rebuilt for loongarch release

* Tue May 28 2024 Pengda Dou <doupengda@loongson.cn> - 8.2.3.3-2
- [Type] other
- [DESC] add support for loongarch64

* Fri Jan 26 2024 Xiaojie Chen <jackxjchen@tencent.com> - 8.2.3.3-1
- Upgrade to upstream version 8.2.3.3
- Fix bogus date in changelog

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.2.1.6-2
- Rebuilt for OpenCloudOS Stream 23.09

* Thu Jul 13 2023 Shuo Wang <abushwang@tencent.com> - 8.2.1.6-1
- update to 8.2.1.6

* Wed Jun 07 2023 rockerzhu <rockerzhu@tencent.com> - 8.2.1.2-6
- Fix kernel source directory for module building.

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.2.1.2-5
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 8.2.1.2-4
- Rebuilt for OpenCloudOS Stream 23

* Thu Feb 23 2023 Miaojun Dong <zoedong@tencent.com> - 8.2.1.2-3
- remove README.md

* Thu Feb 16 2023 rockerzhu <rockerzhu@tencent.com> - 8.2.1.2-2
- Rebuild for kernel 6.1

* Tue Nov 22 2022 rockerzhu <rockerzhu@tencent.com> - 8.2.1.2-1
- Initial build
